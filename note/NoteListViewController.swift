//
//  NoteListViewController.swift
//  note
//
//  Created by Учеба on 13.02.2023.
//

import UIKit

class NoteListViewController: UIViewController {

    @IBOutlet weak var countLAbel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func action(_ sender: Any) {
        performSegue(withIdentifier: "note", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? NoteViewController {
            vc.callback = {title, info in
                self.notes.append(.init(title: title, info: info, date: Date()))
                self.updateCount()
            }
        }
    }
    
    func updateCount() {
        countLAbel.text = "Count Label: " + String(notes.count)
    }
    
    var notes : [Note] = []
}

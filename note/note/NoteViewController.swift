//
//  ViewController.swift
//  note
//
//  Created by Учеба on 13.02.2023.
//

import UIKit

class NoteViewController: UIViewController {
    
    var callback: ( (String, String) -> () )?

    @IBOutlet weak var infoTextView: UITextView!
    @IBOutlet weak var titleTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        titleTextField.placeholder = "Untitled"
        titleTextField.becomeFirstResponder()
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "button"), style: .done, target: self, action: #selector(back))
        navigationController?.navigationBar.tintColor = .black
        
        title = "Note"
    }
    
    @objc private func back(){
        if let title = titleTextField.text, title.isEmpty == false, let info = infoTextView.text, info.isEmpty == false {
            callback?(title, info)
        }
        
        navigationController?.popViewController(animated: true)
    }
}


//
//  NoteModel.swift
//  note
//
//  Created by Учеба on 13.02.2023.
//

import Foundation


struct Note {
    let title: String
    let info: String
    let date: Date
}

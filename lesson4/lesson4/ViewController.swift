//
//  ViewController.swift
//  lesson4
//
//  Created by Учеба on 20.02.2023.
//

import UIKit

protocol ViewControllerDelegate :AnyObject {
    func saveNote(title:String, body:String)
}

class NoteCreateViewController: UIViewController {
    weak var delegage: ViewControllerDelegate?
    
    var callback:((String, String)->())?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    internal var some:String?
    
    var some2: String?
    
    private var some3 : String?
    
    private(set) var some4:String?

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        delegage?.saveNote(title: "hello", body: "worlds")
        
        callback?("hello", "world")
    }
    
    private lazy var textField:UITextField = {
        let text = UITextField()
        text.addTarget(self, action: #selector(changeText), for: .valueChanged)
        return text
    }()
    
    @objc private func changeText() {
        print(textField.text)
    }
    
    
    private lazy var tableView: UITableView = {
        let table = UITableView(frame: .zero, style: .plain)
        table.delegate = self
        table.dataSource = self
        table.register(UINib(nibName: "NoteTableViewCell", bundle: nil), forCellReuseIdentifier: "NoteTableViewCell")
        return table
    }()
    
    private lazy var collectionView:UICollectionView = {
        let collection = UICollectionView(frame: .zero, collectionViewLayout: .init())
        collection.delegate = self
        collection.dataSource = self
        return collection
    }()
}


extension NoteCreateViewController:UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: <#T##String#>, for: indexPath)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        0
    }
        
}

extension NoteCreateViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NoteTableViewCell") as! NoteTableViewCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        0
    }
    
}

class NoteTableViewCell:UITableViewCell {
    
}

class Test{
    func create(){
        var vc = NoteCreateViewController()
        var vc2 = vc
        weak var vc3 = vc
        
        vc.some = "asdasdasd"
        vc.some2 = "asdasd"
        
        
        vc.delegage = self
        
        vc.callback = {title, info in }
    }
    
}

extension Test:ViewControllerDelegate {
    func saveNote(title: String, body: String) {
        
    }
}





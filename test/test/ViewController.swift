//
//  ViewController.swift
//  test
//
//  Created by Учеба on 23.01.2023.
//

import UIKit

class ViewController: UIViewController {
    
    var callback:((String,String)->())?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        titleTextFiled.placeholder = "Заголовок  заметки"
        infoTextView.text = nil
    }
    
    @IBAction func save(_ sender: Any) {
        if let title = titleTextFiled.text, let info = infoTextView.text, title.isEmpty == false, info.isEmpty == false {
         callback?(title,info)
            navigationController?.popViewController(animated: true)
        }
    }
    
    @IBOutlet weak var titleTextFiled: UITextField!
    
    @IBOutlet weak var infoTextView: UITextView!
}


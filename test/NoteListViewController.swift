//
//  NoteListViewController.swift
//  test
//
//  Created by Учеба on 23.01.2023.
//

import UIKit

class NoteListViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "NoteTableViewCell", bundle: nil), forCellReuseIdentifier: "NoteTableViewCell")

        // Do any additional setup after loading the view.
    }
    var viewModels:[NoteModel] = []

    @IBAction func create(_ sender: Any) {
performSegue(withIdentifier: "create", sender: nil)
        
    }
    @IBOutlet weak var tableView: UITableView!
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController = segue.destination as? ViewController {
            viewController.callback = {title, info in
                self.viewModels.append(.init(title: title, info: info))
                self.tableView.reloadData()
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension NoteListViewController:UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NoteTableViewCell") as! NoteTableViewCell
        
        cell.titleLabel.text = viewModels[indexPath.row].title
        cell.infoLabel.text = viewModels[indexPath.row].info
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModels.count
    }
}

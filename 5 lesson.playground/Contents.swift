import UIKit

struct Note :Codable {
    let date: Date
    let id: String
    let title: String
    let info: String
}

class ViewController: UIViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        ///
        segue.destination
        
        /// с чем мы переходим, например если мы вызвали
        /// performSegue(withIdentifier: "some", sender: Double(1)) то в sender будет передан этот объект
        /// для этого можно сделать if let value = sender as? Double {}
        sender
    }
    
    private lazy var textField :UITextField = {
        let text = UITextField()
        /// ставим делегата в текущий класс
        text.delegate = self
        /// с помощью этого свойства мы можем менять то что будет написано на кнопки, в данном случае Готово
        text.returnKeyType = .done
        /// вызывается каждый раз как сменится текст
        text.addTarget(self, action: #selector(textChanged), for: .valueChanged)
        return text
    }()
    
    @objc private func textChanged() {
        /// отменяем таймер, делаем это, чтобы наше действие выполнялось только после того как пользователь закончит ввод текста
        timer?.invalidate()
        /// создаем новый, он выполнится
        timer = Timer(timeInterval: 0.3, repeats: false, block: { timer in
            /// а тут что то делаем позже
        })
    }
    
    /// специальный класс который отвечает за действие которое будет выполнено потом
    private var timer :Timer?

    private var notes: [Note] = []

    /// сохранения в локальном хранилище данных
    private func save() {
        /// создаем особый класс, который будет преобразовывать данные в сырой вид байтов
        let data = try? JSONEncoder().encode(notes)
        /// сохраняем в специальном локальном хранилище
        UserDefaults.standard.set(data, forKey: "Data")
        /// сохраняем хранилище в локальный файл
        UserDefaults.standard.synchronize()
    }
    
    // получаем данные из хранилища
    private func getSavedNotes() -> [Note] {
        /// достаем данные из хранилища
        let data = UserDefaults.standard.object(forKey: "Data") as? Data
        /// создаем преобразователь из сырых данных в файл
        let decoder = JSONDecoder()
        /// проверяем что данные есть
        if let data = data {
            /// пытаемся преборазовать данные. Так как мы хотим получить массив то [Note].self а не просто Note.self в качестве типа в который надо попытаться преобразить
            if let items = try? decoder.decode([Note].self, from: data ) {
                return items
            }
        }
        return []
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        notes = getSavedNotes()
    }
    
    private func showDialog(){
        /// стандартый диалог, для выбора или уведомления
        /// .actionSheet - показывается снизу
        /// .alert - показывается посередине
        let alert = UIAlertController(title: "Заголовок", message: "Сообщение", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel))
        alert.addAction(UIAlertAction(title: "Сохранить", style: .default) { _ in
            self.save()
        })
        present(alert, animated: true)
    }
}


extension ViewController: UITextFieldDelegate {
    /// метод вызывается при нажатии кнопки готово
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        /// мы текущему экрану отображения сообщаем что он закончил редактирование, и нужно скрыть клавиатуру
        view.endEditing(true)
        return true
    }
}

extension ViewController:UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let note = notes[indexPath.row]
        performSegue(withIdentifier: "note", sender: note)
    }
}


var array: [Int] = [1,3,4,5,6,2,3,4,1,5,6,1,8,9,0]

// сортировка по убыванию
array.sorted(by: {$0 > $1})
array.sorted { value1, value2 in
    value1 > value2
}
/// изменения хранимых данных, делаем из чисел строки
array.map{value -> String in
    String(value)
}

/// фильтрация, значение должно быть больше 2
array.filter{ $0 > 2 }
/// суммируем все значение в массива, сначала пишем начальное значение, а затем описываем каждую операцию
array.reduce(0, {$0 + $1})

var sum = 0
/// аналог for value in array {}
array.forEach { value in
    sum += value
}

/// можно делать цепочки операций
let total = array.filter{ $0 > 2 }.sorted{$0 > $1}.map{String($0)}
total

class CollectionViewController: UIViewController {
    private lazy var collection: UICollectionView = {
        let viewLayout : UICollectionViewFlowLayout = .init()
        viewLayout.minimumInteritemSpacing = 16
        viewLayout.minimumLineSpacing = 16
        
        
        let collection = UICollectionView(frame: .zero, collectionViewLayout: viewLayout)
        collection.delegate = self
        collection.dataSource = self
        
        collection.register(UINib(nibName: String(describing: CollectionViewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: CollectionViewCell.self))
        return collection
        
    }()

    var notes : [[Note]] = [[]]
}

extension CollectionViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: CollectionViewCell.self), for: indexPath)
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        notes[section].count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: (collectionView.frame.width - 16 - 16 - 16) / 2, height: 126)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let note = notes[indexPath.section][indexPath.row]
    }
}

class CollectionViewCell: UICollectionViewCell {
    
}

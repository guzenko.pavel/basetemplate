import UIKit

var greeting = "Hello, playground"

var variable : String? = "asdxasd"

if variable != nil {
    print(variable)
}

if let news = variable {
    print(news)
}

func test ()->String {
    
    guard let news = variable else
    {
        print("no news")
        return ""
    }
    
    return news
    
}

variable!


variable?.count

variable ?? "asdasd"

class A{
    var test:String?
    
    init(){
        print("created")
    }
    deinit {
        print("dead")
    }
}


var a: A? = A()

var b:AnyObject? = a


if b is A {
    print("hello")
}

if let news = b as? A {
    print(news)
}



class UICEll : UITableViewCell {
    
}

class UITable:UIView, UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ASDASD") as? UICEll else {
            return UITableViewCell()
        }
        cell.contentView.backgroundColor = .red
        return cell
    }
}


class Glass <T, U> {
    var text:T
    var title:U
    
    var array:[T] = []
    
    init(text:T, title:U){
        self.text = text
        self.title = title
    }
    
    func append(_ value:T){
        array.append(value)
    }
}

var glass = Glass<String, Int>(text: "Asdasd", title: 2)
glass.append("asdasd")

var array: [String] = []

var aaray2 = Array<String>()


